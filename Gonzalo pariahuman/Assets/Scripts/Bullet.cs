﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Rigidbody rb;
    public float speed;
    public Vector3 direction;
    public GameObject pare;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }
    void Move()
    {
        rb.velocity = speed*direction;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (gameObject.CompareTag("pare"))
        {
            Destroy(this.gameObject);
        }
    }
}
