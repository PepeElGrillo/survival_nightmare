﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class obj_municion : MonoBehaviour
{
    public int municion = 0;


    private void OnTriggerEnter(Collider other)
    {
       if (other.gameObject.CompareTag("municion"))
        {
            Destroy(other.gameObject);
            municion += 1;
        }

    }
}
