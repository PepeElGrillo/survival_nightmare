﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Personaje : MonoBehaviour
{
    public Camera FPSCamera;
    public float horizontalSpeed;
    public float verticalSpeed;
    public float speed;
    public Rigidbody rb;
    public Sprite spr1;
    public Sprite spr2;
    public SpriteRenderer spriterenderer;
    public int live;
    float v;
    float h;

    public Transform shootOrigin;
    public GameObject bulletPrefab;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        spriterenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        Cursor.lockState = CursorLockMode.Locked;
        h = horizontalSpeed * Input.GetAxis("Mouse X");
        v = verticalSpeed * Input.GetAxis("Mouse Y");
        transform.Rotate(0, h, 0);
        FPSCamera.transform.Rotate(-v, 0, 0);
        float vertical = Input.GetAxis("Vertical");
        float horizontal = Input.GetAxis("Horizontal");

        rb.velocity = (transform.forward * speed * vertical) + (transform.right * speed * horizontal);


        if(Input.GetKeyDown(KeyCode.Mouse0))
        {
            GameObject obj = Instantiate(bulletPrefab);
            obj.transform.position = shootOrigin.position;
            obj.GetComponent<Bullet>().direction = shootOrigin.transform.forward;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Enemy"))
        {
            live -= 1;

        }
        else
        {
            if (live <= 0)
            {
                Destroy(gameObject);
            }
        }
        
    }
}
